import { exec } from "child_process";

export async function execCmd(
  command: string,
  preserveSpacing?: boolean
): Promise<string> {
  const commandCollapsed = preserveSpacing
    ? command
    : command.replace(/\s+/g, " ");
  return new Promise<string>((resolve, reject) => {
    let res = {
      stdout: "",
      stderr: "",
    };
    exec(commandCollapsed, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }

      resolve(stdout || stderr);
    }).on("exit", (code) => {
      if (code) {
        // reject(new Error(`Command exited with code ${code}`));
        return;
      }
    });
  });
}
