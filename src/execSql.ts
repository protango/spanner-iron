import { execCmd } from "./execCmd";
import { GCloudSqlOutput } from "./gCloudSqlOutput";

const isDDL = (statement: string) =>
  ["CREATE", "ALTER", "DROP"].some((x) => statement.startsWith(x));

export async function execSql<TFields extends string[]>(
  query: string,
  project: string,
  instance: string,
  database: string
): Promise<{ [P in TFields[number]]: string }[]> {
  if (isDDL(query)) {
    await execCmd(`
        gcloud spanner databases ddl update ${database} 
          --instance ${instance} 
          --project ${project} 
          --ddl="${query.replace(/(["$`\\])/g, "\\$1")}"`);
    return [];
  } else {
    const res = await execCmd(`
        gcloud spanner databases execute-sql ${database} 
        --instance ${instance}
        --project ${project}
        --sql="${query.replace(/(["$`\\])/g, "\\$1")}"
        --format=json
  `);
    const raw = JSON.parse(res) as GCloudSqlOutput;

    const fields = raw.metadata.rowType.fields.map((x) => x.name);

    const result: Record<string, string>[] = [];
    for (const row of raw.rows ?? []) {
      const obj: Record<string, string> = {};
      for (let i = 0; i < row.length; i++) {
        obj[fields[i]] = String(row[i]);
      }
      result.push(obj);
    }

    return result as any;
  }
}
