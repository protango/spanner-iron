import { promises as fs } from "fs";
import path from "path";
import { execCmd } from "./execCmd";
import { execSql } from "./execSql";
import { Command } from "commander";
import os from "os";

interface GlobalConfig {
  project: string;
  instance: string;
  database?: string;
}

async function main() {
  const program = new Command();

  const configPath = path.join(os.homedir(), ".spanner-iron.json");
  let globalConfig: GlobalConfig | undefined;
  try {
    const rawCfg = await fs.readFile(configPath, "utf8");
    globalConfig = JSON.parse(rawCfg);
  } catch {}

  program
    .name("spanner-iron")
    .description("Zac's library of GCP Spanner utilities")
    .version("0.0.0");

  program
    .command("config")
    .description(
      "Configure your GCP project and instance for use with this tool."
    )
    .argument("<project>", "GCP project id")
    .argument("<instance>", "Spanner instance id")
    .argument("[database]", "Spanner database id")
    .action(async (project, instance, database) => {
      await fs.writeFile(
        path.join(os.homedir(), ".spanner-iron.json"),
        JSON.stringify({ project, instance, database }, null, 2)
      );
      console.log(`💾 Configuration has been saved`);
    });

  function addGcpOptions(cmd: Command): Command {
    if (globalConfig?.project) {
      cmd.option("--project <id>", "GCP project id", globalConfig.project);
    } else {
      cmd.requiredOption(
        "--project <id>",
        "GCP project id, may be set globally via 'config' command"
      );
    }
    if (globalConfig?.instance) {
      cmd.option(
        "--instance <id>",
        "Spanner instance id",
        globalConfig.instance
      );
    } else {
      cmd.requiredOption(
        "--instance <id>",
        "Spanner instance id, may be set globally via 'config' command"
      );
    }
    if (globalConfig?.database) {
      cmd.option(
        "--database <id>",
        "Spanner database id",
        globalConfig.database
      );
    } else {
      cmd.requiredOption(
        "--database <id>",
        "Spanner database id, may be set globally via 'config' command"
      );
    }

    return cmd;
  }

  program
    .command("nuke")
    .description(
      "Nuke the database, delete all tables and re-create the database from scratch"
    )
    .argument("<SQL_folder>", "folder containing the sql migration files")
    .option(
      "--post-mig <path>",
      "optional sql file to execute at the end of the migration, for example to add test data"
    );

  program.parse();

  return;

  const sqlFolder = "sql";

  const project = "bitwave-dev";
  const instance = "shared-dev";
  const database = "zac_core";
  const testDataFile = "_Testdata.sql";

  const promises: Promise<string[]>[] = [];

  // STAGE 1: Drop all foreign keys
  promises.push(
    execSql<["TABLE_NAME", "CONSTRAINT_NAME"]>(
      `
    SELECT TABLE_NAME, CONSTRAINT_NAME 
    FROM information_schema.TABLE_CONSTRAINTS 
    WHERE TABLE_SCHEMA ='' AND CONSTRAINT_TYPE = 'FOREIGN KEY'`,
      project,
      instance,
      database
    ).then((res) =>
      res.map(
        (x) =>
          `ALTER TABLE ${x.TABLE_NAME} DROP CONSTRAINT ${x.CONSTRAINT_NAME};`
      )
    )
  );

  // STAGE 2: Drop all indexes
  promises.push(
    execSql<["TABLE_NAME", "INDEX_NAME"]>(
      `
      SELECT TABLE_NAME, INDEX_NAME 
      FROM information_schema.INDEXES 
      WHERE TABLE_SCHEMA = '' AND INDEX_TYPE = 'INDEX' AND SPANNER_IS_MANAGED = False`,
      project,
      instance,
      database
    ).then((res) => res.map((x) => `DROP INDEX ${x.INDEX_NAME};`))
  );

  // STAGE 3: Traverse all tables and drop them depth-first
  promises.push(
    (async () => {
      const tables = await execSql<["TABLE_NAME", "PARENT_TABLE_NAME"]>(
        `
      SELECT TABLE_NAME, PARENT_TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = ''
    `,
        project,
        instance,
        database
      );

      const tableDepth: Record<string, number> = {};
      let unProcessedTables = new Set(tables);
      let lastUnprocessedCount = unProcessedTables.size;
      while (unProcessedTables.size > 0) {
        lastUnprocessedCount = unProcessedTables.size;
        for (const table of unProcessedTables) {
          if (!table.PARENT_TABLE_NAME) {
            tableDepth[table.TABLE_NAME] = 0;
            unProcessedTables.delete(table);
          } else if (tableDepth[table.PARENT_TABLE_NAME] !== undefined) {
            tableDepth[table.TABLE_NAME] =
              tableDepth[table.PARENT_TABLE_NAME] + 1;
            unProcessedTables.delete(table);
          }
        }
        if (lastUnprocessedCount === unProcessedTables.size) {
          throw new Error("Circular dependency detected");
        }
      }

      const depthFirstTableSearch = Object.entries(tableDepth)
        .sort((a, b) => b[1] - a[1])
        .map((x) => x[0]);

      return depthFirstTableSearch.map((x) => `DROP TABLE ${x};`);
    })()
  );

  // STAGE 4: Drop all change streams
  promises.push(
    execSql<["CHANGE_STREAM_NAME"]>(
      `
      SELECT CHANGE_STREAM_NAME FROM information_schema.CHANGE_STREAMS
    `,
      project,
      instance,
      database
    ).then((res) =>
      res.map((x) => `DROP CHANGE STREAM ${x.CHANGE_STREAM_NAME};`)
    )
  );

  // STAGE 5: Add data from sql folder
  promises.push(
    (async () => {
      const result = [] as string[];
      const dir = await fs.readdir(sqlFolder);
      dir.sort();
      dir.push(testDataFile);
      for (const fileName of dir) {
        if (!fileName.endsWith(".sql")) {
          continue;
        }
        let fileContent = await fs.readFile(
          fileName !== testDataFile ? path.join(sqlFolder, fileName) : fileName,
          "utf8"
        );
        // Remove comments
        fileContent = fileContent.replace(/--.*$/gm, "");
        // Collapse whitespace
        fileContent = fileContent.replace(/\s+/g, " ").trim();
        // split by semicolon
        const fileStatements = fileContent.split(";").map((x) => x.trim());
        result.push(...fileStatements);
      }
      return result;
    })()
  );

  console.log("🔎 Gathering schema information");
  const statements = (await Promise.all(promises)).flat();

  // STAGE 6: Execute!
  const isDDL = (statement: string) =>
    ["CREATE", "ALTER", "DROP"].some((x) => statement.startsWith(x));
  let ddlAggregator = "";
  for (const statement of statements) {
    if (statement.trim() === "") {
      continue;
    }
    if (isDDL(statement)) {
      ddlAggregator += statement + (statement.endsWith(";") ? "" : ";") + "\n";
    } else {
      if (ddlAggregator) {
        console.log("✏️ Executing DDL statements");
        await execSql(ddlAggregator, project, instance, database);
        console.log("✅ Success");
        ddlAggregator = "";
      }
      console.log("📝 Executing DML statement");
      await execSql(statement, project, instance, database);
      console.log("✅ Success");
    }
  }

  if (ddlAggregator) {
    console.log("✏️ Executing DDL statements");
    await execSql(ddlAggregator, project, instance, database);
    console.log("✅ Success");
  }
}

main();
