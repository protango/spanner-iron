export interface GCloudSqlOutput {
  metadata: Metadata;
  rows: Array<string[]>;
}

interface Metadata {
  rowType: RowType;
  transaction: Record<string, unknown>;
  undeclaredParameters: Record<string, unknown>;
}

interface RowType {
  fields: Field[];
}

interface Field {
  name: string;
  type: Type;
}

interface Type {
  code: string;
}
