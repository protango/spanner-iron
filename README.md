# spanner-iron

A flyway replacement that just re-creates the database from scratch based on your migrations.

"Irons" out the wrinkles in your database for a fresh start
